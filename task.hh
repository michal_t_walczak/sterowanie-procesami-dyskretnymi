/*
 * Nazwa pliku: task.hh
 */

#ifndef TASK_HH
#define TASK_HH

#include<iostream>

class Task
{
private:
    int m_id;
    float m_head;
    float m_body;
    float m_tail;
    float m_remaining_run_time;
public:
    Task();
    Task(int id, float head, float body, float tail);
    int getId() { return m_id; }
    float getHead() const { return m_head; }
    float getBody() const { return m_body; }
    float getTail() const { return m_tail; }
    void setHead(float head) { m_head = head; }
    void setBody(float body) { m_body = body; }
    void setTail(float tail) { m_tail = tail; }
    void updateRemainingRunTime(float time);
    float getRemainingRunTime() const { return m_remaining_run_time; }
    ~Task() {};
    friend std::ostream& operator<<(std::ostream& os, const Task& t);
};

#endif