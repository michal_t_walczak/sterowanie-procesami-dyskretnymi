/*
 * Nazwa pliku: main.cpp
 */

#include<fstream>
#include<iostream>
#include<vector>
#include<ctime>
#include<chrono>
#include"task.hh"
#include"loader.hh"
#include"heap_min.hh"
#include"heap_max.hh"
#include"solver.hh"

int main()
{   
    std::ifstream in_file;
    in_file.open("./dane_wejsciowe/dane.1");
    std::vector<Task> dane_1 = Loader::loadData(in_file);
    in_file.close();
    in_file.open("./dane_wejsciowe/dane.2");
    std::vector<Task> dane_2 = Loader::loadData(in_file);
    in_file.close();
    in_file.open("./dane_wejsciowe/dane.3");
    std::vector<Task> dane_3 = Loader::loadData(in_file);
    in_file.close();
    in_file.open("./dane_wejsciowe/dane.4");
    std::vector<Task> dane_4 = Loader::loadData(in_file);
    in_file.close();

    float delivery_time = 0.0;
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    std::vector<Task> rezultat = Solver<Task>::solve(dane_1);
    std::cout << "C_max1: " << Solver<Task>::getMaxDeliveryTime() << std::endl;
    delivery_time += Solver<Task>::getMaxDeliveryTime();
    Solver<Task>::solve(dane_2);
    std::cout << "C_max2: " << Solver<Task>::getMaxDeliveryTime() << std::endl;
    delivery_time += Solver<Task>::getMaxDeliveryTime();
    Solver<Task>::solve(dane_3);
    std::cout << "C_max3: " << Solver<Task>::getMaxDeliveryTime() << std::endl;
    delivery_time += Solver<Task>::getMaxDeliveryTime();
    Solver<Task>::solve(dane_4);
    std::cout << "C_max4: " << Solver<Task>::getMaxDeliveryTime() << std::endl;
    delivery_time += Solver<Task>::getMaxDeliveryTime();

    end = std::chrono::system_clock::now();
    std::chrono::duration<double> computing_time_sec = end - start;
    std::cout << "Total delivery time: " << delivery_time << std::endl;
    std::cout << "Computing time: " << computing_time_sec.count() << std::endl;
}
