/*
 * Nazwa pliku: task.cpp
 */

#include<iostream>
#include"task.hh"

Task::Task()
{
    m_id = -1;
    m_head = -1;
    m_body = -1;
    m_tail = -1;
}

Task::Task(int id, float head, float body, float tail)
{
    m_id = id;
    m_head = head;
    m_body = body;
    m_tail = tail;
}

void Task::updateRemainingRunTime(float time)
{
    m_remaining_run_time -= time;
}

std::ostream& operator<<(std::ostream& os, const Task& task)
{
    os << task.m_id << " " << task.m_head << " " << task.m_body << " " << task.m_tail << std::endl;
    return os;
}