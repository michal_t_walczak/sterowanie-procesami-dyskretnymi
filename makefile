lab1_kasprzak_walczak : main.o loader.o task.o
	g++ -pedantic -Wall -std=c++11 main.o loader.o task.o -o lab1_kasprzak_walczak

main.o : main.cpp loader.hh task.hh heap_min.hh heap_max.hh solver.hh
	g++ -pedantic -Wall -std=c++11 -c main.cpp
loader.o : loader.cpp task.hh loader.hh
	g++ -pedantic -Wall -std=c++11 -c loader.cpp
task.o : task.cpp task.hh 
	g++ -pedantic -Wall -std=c++11 -c task.cpp

clean :
	rm lab1_kasprzak_walczak main.o loader.o task.o
