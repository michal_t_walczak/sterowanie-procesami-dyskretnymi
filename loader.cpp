/*
 * Nazwa pliku: loader.cpp
 */

#include<fstream>
#include<vector>
#include<iostream>
#include"task.hh"
#include"loader.hh"

std::vector<Task> Loader::loadData(std::ifstream& input_file)
{
    int tasks = 0;
    input_file >> tasks;
    std::vector<Task> ret {};
    int i = 0;
    float head = 0, body = 0, tail = 0;
    while(i < tasks)
    {
        input_file >> head >> body >> tail;
        ret.push_back(Task(i, head, body, tail));
        i++;
    }
    return ret;
}