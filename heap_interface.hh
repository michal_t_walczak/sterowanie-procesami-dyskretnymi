/*
 * Nazwa pliku: heap_interface.hh
 */

#ifndef HEAP_INTERFACE_HH
#define HEAP_INTERFACE_HH

#include<vector>
#include<iostream>

template<typename Object>
class Heap
{
protected:
    int m_nodes;
    int m_current_position;
    std::vector<Object> m_content;
public:
    Heap();
    int getNodes() const { return m_nodes; }
    int getCurrentPosition() const { return m_current_position; }
    virtual void pushHeap(Object element) = 0;
    Object getRoot() const;
    virtual Object popHeap() = 0;
    bool isEmpty();
    void showHeapVector();
    virtual ~Heap() {}
};

template<typename Object>
Heap<Object>::Heap()
{
    m_current_position = 0;
    m_nodes = 0;   
}

template<typename Object>
Object Heap<Object>::getRoot() const
{
    return m_content[0];
}

template<typename Object>
bool Heap<Object>::isEmpty()
{
    if(m_nodes == 0) return true;
    else return false;
}

/*
 * Uwaga: aby wykorzystac ta metode konieczne jest uprzednie przeciazenie
 * operaotra << dla klasy obiektow przechowywanych na kopcu
 */
template<typename Object>
void Heap<Object>::showHeapVector()
{
    for(unsigned int i = 0; i < m_content.size(); i++)
        std::cout << m_content.at(i);
}

#endif