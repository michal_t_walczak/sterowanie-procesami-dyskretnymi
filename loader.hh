/*
 * Nazwa pliku: loader.hh
 */

#ifndef LOADER_HH
#define LOADER_HH

#include<fstream>
#include<vector>
#include"task.hh"

class Loader
{
    public:
        static std::vector<Task> loadData(std::ifstream& input_file);
};

#endif