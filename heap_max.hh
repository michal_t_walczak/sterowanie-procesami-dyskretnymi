/*
 * Nazwa pliku: heap_max.hh
 */

#ifndef HEAP_MAX_HH
#define HEAP_MAX_HH

#include<vector>
#include<algorithm>
#include"heap_interface.hh"

template<typename Object>
class HeapMax : public Heap<Object>
{
private:
    int comp(Object t1, Object t2) { if(t1.getTail() > t2.getTail()) return 1; else if (t1.getTail() == t2.getTail()) return 0; else return -1; }
public:
    HeapMax() : Heap<Object>() {}
    HeapMax(std::vector<Object> elements);
    virtual void pushHeap(Object element);
    virtual Object popHeap();
    virtual ~HeapMax() {}
};

template<typename Object>
HeapMax<Object>::HeapMax(std::vector<Object> elements) : Heap<Object>()
{
    this->m_current_position = 0;
    for(unsigned int i = 0; i < elements.size(); i++)
    {
        pushHeap(elements.at(i));
    }
    this->m_nodes = this->m_content.size();
}

template<typename Object>
void HeapMax<Object>::pushHeap(Object element)
{
    this->m_content.push_back(element);
    int pos = this->m_current_position;
    while(pos != 0 && comp(this->m_content[(pos-1)/2], this->m_content[pos]) < 0)
    {
        std::swap(this->m_content[(pos-1)/2], this->m_content[pos]);
        pos = (pos-1)/2;
    }
    this->m_current_position += 1;
    this->m_nodes = this->m_content.size();
}

template<typename Object>
Object HeapMax<Object>::popHeap() 
{
    Object ret;
    if(this->m_current_position == 0)
        std::cout << "Kopiec jest pusty" << std::endl;
    else
    {
        ret = this->m_content[0];
        this->m_content[0] = this->m_content[(this->m_current_position--)-1];
        this->m_content.pop_back();
        /* Przywracanie wlasciwosci kopca */
        int processed_node = 0;
        while(true)
        {
            if(2*processed_node+1 > this->m_current_position) break;
            else if(2*processed_node+2 > this->m_current_position) 
            {
                if(comp(this->m_content[processed_node], this->m_content[2*processed_node+1]) >= 0) break;
                std::swap(this->m_content[processed_node], this->m_content[2*processed_node+1]);
                processed_node = 2*processed_node+1;
            }
            else if(comp(this->m_content[2*processed_node+1], this->m_content[2*processed_node+2]) > 0) 
            {
                if(comp(this->m_content[processed_node], this->m_content[2*processed_node+1]) >= 0) break;
                std::swap(this->m_content[processed_node], this->m_content[2*processed_node+1]);
                processed_node = 2*processed_node+1;
            }
            else
            {
                if(comp(this->m_content[processed_node], this->m_content[2*processed_node+2]) >= 0) break;
                std::swap(this->m_content[processed_node], this->m_content[2*processed_node+2]);
                processed_node = 2*processed_node+2;
            }
        }
    }
    this->m_nodes = this->m_content.size();
    return ret;
}

#endif