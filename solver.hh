/*
 * Nazwa pliku: solver.hh
 */

#ifndef SOLVER_HH
#define SOLVER_HH

#include<vector>
#include<algorithm>
#include"heap_min.hh"
#include"heap_max.hh"

template<typename Object>
class Solver
{
private:
    Solver();
    static float m_time;
    static float m_max_delivery_time;
    static int m_position;
    static HeapMax<Object> m_ready_tasks;
    static HeapMin<Object> m_unordered_tasks;
public:
    static float getMaxDeliveryTime() { return m_max_delivery_time; }
    static std::vector<Object> solveSchrage(std::vector<Object> tasks);
    static std::vector<Object> solveSortRQ(std::vector<Object> tasks);
    static std::vector<Object> solveMaxInMid(std::vector<Object> tasks);
    static std::vector<Object> solve(std::vector<Object> tasks);
};

template<typename Object>
float Solver<Object>::m_time = 0;
template<typename Object>
float Solver<Object>::m_max_delivery_time = 0;
template<typename Object>
int Solver<Object>::m_position = 0;
template<typename Object>
HeapMax<Object> Solver<Object>::m_ready_tasks {};
template<typename Object>
HeapMin<Object> Solver<Object>::m_unordered_tasks {};

template<typename Object>
Solver<Object>::Solver()
{
    m_time = 0;
    m_position = 0;
    m_max_delivery_time = 0;
    m_ready_tasks = HeapMax<Object>();
    m_unordered_tasks = HeapMin<Object>();
}

template<typename Object>
std::vector<Object> Solver<Object>::solveSchrage(std::vector<Object> tasks)
{
    Solver();
    std::vector<Object> ret {};
    m_unordered_tasks = HeapMin<Object>(tasks);
    while(!m_ready_tasks.isEmpty() || !m_unordered_tasks.isEmpty())
    {
        while(!m_unordered_tasks.isEmpty() && m_unordered_tasks.getRoot().getHead() <= m_time)
        {
            Object e = m_unordered_tasks.popHeap();
            m_ready_tasks.pushHeap(e);
        }
        if(m_ready_tasks.isEmpty())
        {
            m_time = m_unordered_tasks.getRoot().getHead();
            continue;
        }
        Object e = m_ready_tasks.popHeap();
        ret.push_back(e);
        m_position += 1;
        m_time += e.getBody();
        m_max_delivery_time < (m_time+e.getTail()) ? m_max_delivery_time = m_time+e.getTail() : m_max_delivery_time;
    }
    return ret;
}

template<typename Object>
std::vector<Object> Solver<Object>::solveSortRQ(std::vector<Object> tasks)
{
    Solver();
    std::vector<Object> ret {};
    HeapMax<Object> q_greater = HeapMax<Object>();
    HeapMin<Object> r_greater = HeapMin<Object>();
    for(auto t : tasks) { if(t.getHead() > t.getTail()) q_greater.pushHeap(t); else r_greater.pushHeap(t); }
    while(!r_greater.isEmpty()) { ret.push_back(r_greater.popHeap()); }
    while(!q_greater.isEmpty()) { ret.push_back(q_greater.popHeap()); }
    for(auto e : ret) 
    {
        m_position += 1;
        if(e.getHead() > m_time) m_time = e.getHead();
        m_time += e.getBody(); 
        m_max_delivery_time < (m_time+e.getTail()) ? m_max_delivery_time = m_time+e.getTail() : m_max_delivery_time; 
    }
    return ret;
}

template<typename Object>
std::vector<Object> Solver<Object>::solveMaxInMid(std::vector<Object> tasks)
{
    Solver();
    auto it = std::max_element(tasks.begin(), tasks.end(),
			      [](const Task & t1, const Task & t2) -> bool
			      { return t1.getTail()+t1.getHead() < t2.getTail()+t2.getHead(); });
    auto max = *it;
    tasks.erase(it);
  
    std::sort(tasks.begin(), tasks.end(),
	    [](const Task & t1, const Task & t2) -> bool
	    { return t1.getBody() < t2.getBody(); });

    float sum = 0;
    for(it = tasks.begin() ; it!= tasks.end(); ++it)
    {
        sum += it->getBody();
        if(sum >= max.getHead())
	    {
	        tasks.insert(++it, max);
	        break;
	    }
    }
    for(auto e : tasks) 
    {
        m_position += 1;
        if(e.getHead() > m_time) m_time = e.getHead();
        m_time += e.getBody(); 
        m_max_delivery_time < (m_time+e.getTail()) ? m_max_delivery_time = m_time+e.getTail() : m_max_delivery_time; 
    }
    return tasks;
}

template<typename Object>
std::vector<Object> Solver<Object>::solve(std::vector<Object> tasks)
{
    std::vector< std::pair< std::vector<Object>, float> > ans;
    ans.push_back({Solver<Object>::solveSchrage(tasks), Solver<Object>::getMaxDeliveryTime()});
    ans.push_back({Solver<Object>::solveSortRQ(tasks), Solver<Object>::getMaxDeliveryTime()});
    ans.push_back({Solver<Object>::solveMaxInMid(tasks), Solver<Object>::getMaxDeliveryTime()});
    auto it = std::min_element(ans.begin(), ans.end(),
			      [](const std::pair< std::vector<Object>, float>  & a1, const std::pair< std::vector<Object>, float> & a2) -> bool
			      { return a1.second < a2.second; });
    m_max_delivery_time = it->second;
    for(auto e : it->first) std::cout << e.getId()+1 << " ";
    return it->first;
}

#endif
